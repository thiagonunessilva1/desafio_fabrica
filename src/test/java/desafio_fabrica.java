import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class desafio_fabrica {

    private WebDriver driver;

    @Before
    public void abrir() {
        System.setProperty("webdriver.gecko.drover","C:/Windows/geckodriver.exe");
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.get("https://testpages.herokuapp.com/styled/basic-html-form-test.html");
    }

    @After
    public void sair() {driver.quit();}

    @Test
    public void InserirUsername() throws InterruptedException {
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[1]/td/input")).sendKeys("Thiago Nunes");  //INSERIR USUÁRIO
        Thread.sleep(500);
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[2]/td/input")).sendKeys("alunounipe123");  //INSERIR SENHA
        Thread.sleep(500);
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[5]/td/input[2]")).click();    //CLICAR NA CHECKBOX 2
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[5]/td/input[3]")).click();    //CLICAR NA CHECKBOX 3 PARA DESMARCAR
        Thread.sleep(500);
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[6]/td/input[3]")).click();  //CLICAR EM RÁDIO 3
        Thread.sleep(500);
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[7]/td/select/option[3]")).click();   //SELECIONAR VALOR 3
        Thread.sleep(500);
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[9]/td/input[2]")).click(); //CLICAR EM SUBMIT
        Thread.sleep(3000);
        Assert.assertEquals("Thiago Nunes",driver.findElement(By.id("_valueusername")).getText());  //VALIDAR VALOR DO USUÁRIO
        Assert.assertEquals("alunounipe123",driver.findElement(By.id("_valuepassword")).getText());  //VALIDAR VALOR DA SENHA
        Assert.assertEquals("cb2",driver.findElement(By.id("_valuecheckboxes0")).getText());  //VALIDAR MARCAÇÃO DO CHECKBOX
        Assert.assertEquals("rd3",driver.findElement(By.id("_valueradioval")).getText());  //VALIDAR MARCAÇÃO DO RÁDIO
        Assert.assertEquals("ms3",driver.findElement(By.id("_valuemultipleselect0")).getText());  //VALIDAR SELEÇÃO DO MULTIPLE SELECT
        Assert.assertEquals("submit",driver.findElement(By.id("_valuesubmitbutton")).getText());//VALIDAR O SUBMIT
        Thread.sleep(500);
    }
}
